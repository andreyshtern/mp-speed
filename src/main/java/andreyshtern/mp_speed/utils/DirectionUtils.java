package andreyshtern.mp_speed.utils;

import andreyshtern.mp_speed.structs.MovementDirections;

public class DirectionUtils {
    public static MovementDirections getOppositeDirection(MovementDirections dir){
        if(dir == MovementDirections.HORIZONTAL){
            return MovementDirections.VERTICAL;
        }
        return MovementDirections.HORIZONTAL;
    }
    public static boolean isHorizontal(MovementDirections dir){
        return dir == MovementDirections.HORIZONTAL;
    }
}
