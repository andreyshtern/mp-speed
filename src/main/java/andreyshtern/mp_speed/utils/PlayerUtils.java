package andreyshtern.mp_speed.utils;

import keelfy.klibrary.utils.KVector;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class PlayerUtils {
    public static int getBlocksUpBeforeAir(World world, EntityPlayer player, int yMovement){
        KVector vector = new KVector(player);

        for(int i = 0; i < yMovement; i++){
            Block block = world.getBlock(vector.getBlockX(), vector.getBlockY()+i, vector.getBlockZ());


            if(block == Blocks.air){
                return i + 1;
            }
        }

        return 0;
    }
    public static int getBlocksDownBeforeAir(World world, EntityPlayer player, int yMovement){
        KVector vector = new KVector(player);

        for(int i = 0; i > yMovement; i--){
            Block block = world.getBlock(vector.getBlockX(), vector.getBlockY()+i, vector.getBlockZ());

            if(block == Blocks.air){
                return i + 1;
            }
        }
        return 0;
    }
}
