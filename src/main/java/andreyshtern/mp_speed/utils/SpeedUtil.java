package andreyshtern.mp_speed.utils;

import andreyshtern.mp_speed.MpSpeed;
import andreyshtern.mp_speed.network.MPSServerPackets;
import andreyshtern.mp_speed.structs.MovementDirections;
import andreyshtern.mp_speed.structs.MovementTypes;
import andreyshtern.mp_speed.structs.Speeds;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import andreyshtern.mp_speed.props.PlayerSpeedProps;

public class SpeedUtil {

    private static Speeds fromServer = new Speeds(0.05f, 1f,0.1f);
    public static void setFromServer(Speeds s){
        fromServer = s;

    }

    public static void setPlayerSpeed(EntityPlayer player, MovementTypes type, MovementDirections dir, float value){
        if(FMLCommonHandler.instance().getSide() == Side.CLIENT){

            if(dir == MovementDirections.HORIZONTAL && type != MovementTypes.SWIM){
                MpSpeed.kNetwork.sendToServer(MPSServerPackets.MODIFY_CAPABILITIES,player.getDisplayName(),type.name(),(int)value);

            } else {
                MpSpeed.kNetwork.sendToServer(MPSServerPackets.SET_SPEED,player.getDisplayName(),type.name(),dir.name(),(int)value);
            }
        } else {
            if(!MpSpeed.server)
                return;
            if(dir == MovementDirections.VERTICAL){
                PlayerSpeedProps.get(player).getSaved().setSpeed(type,value);
            } else {

                PlayerSpeedProps.get(player).getSaved().setHCaps(type, value);

            }


        }

    }
    public static float getPlayerSpeed(EntityPlayer player, MovementTypes types, MovementDirections dir){
        if(FMLCommonHandler.instance().getSide() == Side.CLIENT){
            if(dir == MovementDirections.HORIZONTAL && types == MovementTypes.WALK){
                if(fromServer.getHCaps(types)==0)
                    return 1;
                return fromServer.getHCaps(types);
            }

            if(dir == MovementDirections.VERTICAL)
                return fromServer.getSpeed(types);



        } else {
            if(!MpSpeed.server)
                return 0;
            if(dir == MovementDirections.VERTICAL)
                return PlayerSpeedProps.get(player).getSaved().getSpeed(types);
        }
        return getSpeedFromType(player, types);


    }
    @SideOnly(Side.SERVER)
    public static Speeds getSpeeds(EntityPlayer p){

        return PlayerSpeedProps.get(p).getSaved();


    }

    public static float getSpeedFromType(EntityPlayer player, MovementTypes type){
        if(FMLCommonHandler.instance().getSide() == Side.SERVER && MpSpeed.server){
            switch (type){
                case FLY:

                    return player.capabilities.getFlySpeed() * 10 + 1;
                case WALK:
                    try {
                        double attributeSpeed = player.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.movementSpeed).getModifier(MpSpeed.uuid).getAmount();

                        return (float) (attributeSpeed + 1);
                    } catch (NullPointerException e){
                        return 1;
                    }
                case SWIM:
                    return PlayerSpeedProps.get(player).getSaved().getSpeed(MovementTypes.SWIM);
            }
        }

        return fromServer.getHCaps(type);
    }



}
