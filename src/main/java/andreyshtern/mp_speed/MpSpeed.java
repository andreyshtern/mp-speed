package andreyshtern.mp_speed;

import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import keelfy.klibrary.network.KNetwork;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import org.apache.logging.log4j.Logger;

import andreyshtern.mp_speed.proxy.CommonProxy;

import java.util.UUID;

@Mod(modid = MpSpeed.MODID, version = MpSpeed.VERSION)
public class MpSpeed
{
    public static final UUID uuid = UUID.randomUUID();
    public static final String MODID = "mp_speed";
    public static final String VERSION = "1.0.2.3";
    @Mod.Instance(MpSpeed.MODID)
    public static MpSpeed INSTANCE;
    @SidedProxy(serverSide = "andreyshtern.mp_speed.proxy.ServerProxy", clientSide = "andreyshtern.mp_speed.proxy.ClientProxy")
    public static CommonProxy proxy;
    public static KNetwork kNetwork;
    public static final boolean server = true;
    public static Logger LOGGER = FMLLog.getLogger();
    @SideOnly(Side.SERVER)
    private static String linkUrl;
    private final AttributeModifier walkMod = new AttributeModifier(uuid,"walkMod",2,1);
    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        proxy.preInit(event);
    }
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        kNetwork = new KNetwork(MpSpeed.MODID);
		proxy.init(event);
    }
    @EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
        proxy.postInit(event);
    }

    @EventHandler
    public void serverStarting(FMLServerStartingEvent event){
        proxy.serverStarting(event);

    }
}
