package andreyshtern.mp_speed.proxy;

import andreyshtern.mp_speed.MpSpeed;
import andreyshtern.mp_speed.commands.CommandSpeed;
import andreyshtern.mp_speed.event.ServerEventHandler;
import andreyshtern.mp_speed.network.ServerPacketHandler;
import andreyshtern.mp_speed.props.PlayerSpeedProps;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;

public class ServerProxy extends CommonProxy {

    private static String shopUrl;

    public static String getShopUrl() {
        return shopUrl;
    }

    public void preInit(FMLPreInitializationEvent event)
    {
        super.preInit(event);
        if(!MpSpeed.server)
            return;
        FMLCommonHandler.instance().bus().register(new ServerEventHandler());
        MinecraftForge.EVENT_BUS.register(new ServerEventHandler());

        //Config load
        Configuration config = new Configuration(event.getSuggestedConfigurationFile());
        config.load();
        shopUrl = config.get(Configuration.CATEGORY_GENERAL,"shopUrl", "https://moonpower.cc/shop/role").getString();
        config.save();
    }

    public void init(FMLInitializationEvent event)
    {
        super.init(event);
        if(!MpSpeed.server)
            return;
        ServerPacketHandler.register();
        PlayerSpeedProps.register();
    }

    public void postInit(FMLPostInitializationEvent event)
    {
        super.postInit(event);
    }

    public void serverStarting(FMLServerStartingEvent event){

        event.registerServerCommand(new CommandSpeed());

    }
}
