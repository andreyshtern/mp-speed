package andreyshtern.mp_speed.proxy;

import andreyshtern.mp_speed.event.CommonEventHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;

import andreyshtern.mp_speed.props.PlayerSpeedProps;

public class CommonProxy {

    public void preInit(FMLPreInitializationEvent event)
    {
        PlayerSpeedProps.register();
        FMLCommonHandler.instance().bus().register(new CommonEventHandler());

    }

    public void init(FMLInitializationEvent event)
    {

    }

    public void postInit(FMLPostInitializationEvent event)
    {
    }

    public void serverStarting(FMLServerStartingEvent event){

    }
}
