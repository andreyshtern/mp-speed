package andreyshtern.mp_speed.proxy;

import andreyshtern.mp_speed.entity.FakePlayerClient;
import andreyshtern.mp_speed.event.ClientEventHandler;
import andreyshtern.mp_speed.network.ClientPacketHandler;
import andreyshtern.mp_speed.render.CustomRenderPlayer;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;

import java.io.File;

public class ClientProxy extends CommonProxy {
    public void preInit(FMLPreInitializationEvent event)
    {
        super.preInit(event);
        FMLCommonHandler.instance().bus().register(new ClientEventHandler());
        MinecraftForge.EVENT_BUS.register(new ClientEventHandler());
        RenderingRegistry.registerEntityRenderingHandler(FakePlayerClient.class,new CustomRenderPlayer());
    }

    public void init(FMLInitializationEvent event)
    {
        super.init(event);
        ClientPacketHandler.register();
    }

    public void postInit(FMLPostInitializationEvent event)
    {
        super.postInit(event);
    }

}
