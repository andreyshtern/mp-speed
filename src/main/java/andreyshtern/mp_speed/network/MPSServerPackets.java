package andreyshtern.mp_speed.network;

public enum  MPSServerPackets {
    MOVE_ENTITY, SET_SPEED, MODIFY_CAPABILITIES;
}
