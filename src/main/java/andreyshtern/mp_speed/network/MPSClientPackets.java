package andreyshtern.mp_speed.network;

public enum MPSClientPackets {
    OPEN_GUI, SEND_GUI, SEND_SPEED, SET_SLIDER, SET_SPEED_REMOTE;
}
