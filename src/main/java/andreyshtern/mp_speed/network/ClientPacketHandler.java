package andreyshtern.mp_speed.network;

import andreyshtern.mp_speed.MpSpeed;
import andreyshtern.mp_speed.gui.GuiController;
import andreyshtern.mp_speed.gui.GuiSpeed;
import andreyshtern.mp_speed.structs.*;
import com.mojang.authlib.GameProfile;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.FMLNetworkEvent;
import io.netty.buffer.ByteBuf;
import keelfy.klibrary.network.KPacketReceiver;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import andreyshtern.mp_speed.entity.FakePlayerClient;
import andreyshtern.mp_speed.utils.SpeedUtil;

import java.util.UUID;


public enum  ClientPacketHandler {
    INSTANCE;
    public static void register(){
        MpSpeed.kNetwork.registerPacketHandler(INSTANCE);
    }
    @SubscribeEvent
    public void onClientPacket(final FMLNetworkEvent.ClientCustomPacketEvent event) {
        final KPacketReceiver.ClientPacketReceiver receiver;
        try {
            receiver = new KPacketReceiver.ClientPacketReceiver(event);

        } catch (NegativeArraySizeException e){
            return;
        }
        final MPSClientPackets packet = MPSClientPackets.values()[receiver.getPacketNumber()];
        final ByteBuf buffer = receiver.getBuffer();
        if (receiver.getPlayer() == null)
            return;

        switch (packet) {
            case SEND_GUI:
                String name = ByteBufUtils.readUTF8String(buffer);
                MaxSpeeds speeds = MaxSpeeds.NBTToMaxSpeeds(ByteBufUtils.readTag(buffer));
                String linkUrl = ByteBufUtils.readUTF8String(buffer);
                boolean isOp = buffer.readBoolean();
                NBTTagCompound compound = ByteBufUtils.readTag(buffer);
                UUID uuid = UUID.fromString(ByteBufUtils.readUTF8String(buffer));
                EntityPlayer player = new FakePlayerClient(Minecraft.getMinecraft().theWorld,new GameProfile(uuid,name));
                player.readEntityFromNBT(compound);
                GuiController.setPlayer(player);
                GuiController.setNick(name);
                GuiController.setSpeeds(speeds);
                GuiController.setLinkUrl(linkUrl);
                GuiController.setIsOp(isOp);

                GuiScreen currentScr = Minecraft.getMinecraft().currentScreen;
                if(currentScr instanceof GuiSpeed){
                    currentScr.initGui();
                }
                break;
            case OPEN_GUI:
                Minecraft.getMinecraft().displayGuiScreen(new GuiSpeed());
                break;
            case SEND_SPEED:

                SpeedUtil.setFromServer(NBTUtils.getSpeedsFromNBT(ByteBufUtils.readTag(buffer)));

                break;
            case SET_SLIDER:
                GuiSpeed.mSlider.setValue(buffer.readDouble());
                break;
            case SET_SPEED_REMOTE:
                MovementDirections dir = MovementDirections.valueOf(ByteBufUtils.readUTF8String(buffer));
                MovementTypes type = MovementTypes.valueOf(ByteBufUtils.readUTF8String(buffer));
                int value = buffer.readInt();
                SpeedUtil.setPlayerSpeed(receiver.getPlayer(),type,dir,value);
                break;


        }
    }

}
