package andreyshtern.mp_speed.network;

import andreyshtern.mp_speed.MpSpeed;
import andreyshtern.mp_speed.perm.Permissions;
import andreyshtern.mp_speed.structs.MovementDirections;
import andreyshtern.mp_speed.structs.MovementTypes;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.FMLNetworkEvent;
import io.netty.buffer.ByteBuf;
import keelfy.klibrary.network.KPacketReceiver;
import keelfy.klibrary.server.KServerUtils;
import keelfy.mppermit.permission.Permission;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagFloat;
import andreyshtern.mp_speed.utils.SpeedUtil;

import static andreyshtern.mp_speed.MpSpeed.uuid;

public enum  ServerPacketHandler {
    INSTANCE;
    public static void register(){
        MpSpeed.kNetwork.registerPacketHandler(INSTANCE);
    }
    @SubscribeEvent
    public void onServerPacket(final FMLNetworkEvent.ServerCustomPacketEvent event) {
        if(!MpSpeed.server)
            return;
        final KPacketReceiver.ServerPacketReceiver receiver;

        receiver = new KPacketReceiver.ServerPacketReceiver(event);
        final MPSServerPackets packet = MPSServerPackets.values()[receiver.getPacketNumber()];
        final ByteBuf buffer = receiver.getBuffer();
        switch (packet) {
            case MOVE_ENTITY:
                receiver.getPlayer().moveEntity(buffer.readDouble(),buffer.readDouble(),buffer.readDouble());
                break;
            case SET_SPEED:

                setSpeed(receiver,buffer);
                break;
            case MODIFY_CAPABILITIES:
                modifyCapabilities(receiver,buffer);
                break;

        }





    }
    private void setSpeed(KPacketReceiver.ServerPacketReceiver receiver, ByteBuf buffer){
        if(!MpSpeed.server)
            return;
        String nickname = ByteBufUtils.readUTF8String(buffer);
        MovementTypes type = MovementTypes.valueOf(ByteBufUtils.readUTF8String(buffer));
        MovementDirections dir = MovementDirections.valueOf(ByteBufUtils.readUTF8String(buffer));
        int speed = buffer.readInt();
        SpeedUtil.setPlayerSpeed(receiver.getPlayer().getEntityWorld().getPlayerEntityByName(nickname),
                type,dir,speed);
    }
    private void modifyCapabilities(KPacketReceiver.ServerPacketReceiver receiver, ByteBuf buffer){
        if(!MpSpeed.server)
            return;
        EntityPlayerMP player = KServerUtils.matchPlayer(receiver.getPlayer(),ByteBufUtils.readUTF8String(buffer));


        NBTTagCompound tagCompound = new NBTTagCompound();
        player.capabilities.writeCapabilitiesToNBT(tagCompound);
        MovementTypes type = MovementTypes.valueOf(ByteBufUtils.readUTF8String(buffer));
        Permission perm = Permissions.SPEED.getPermission(player,false);
        int speed = buffer.readInt();
        if(KServerUtils.isOp(receiver.getPlayer())|| perm.getMetadata().get(type.name().toLowerCase()+"_horizontal").asInt()>=speed){

            if(type == MovementTypes.WALK){
                if(player.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.movementSpeed).getModifier(uuid)!=null)
                    player.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.movementSpeed).removeModifier(
                            player.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.movementSpeed).getModifier(uuid)
                    );
                if(speed != 1){
                    player.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.movementSpeed).applyModifier(new AttributeModifier(uuid,"walkMod",speed,2));

                }
//                tagCompound.getCompoundTag("abilities").setTag("walkSpeed", new NBTTagFloat(0.1f*speed));
                SpeedUtil.setPlayerSpeed(player,type,MovementDirections.HORIZONTAL,speed);
            } else if(type == MovementTypes.FLY){

                float sp = speed == 1 ? 0.05f*speed : 0.03f*speed;

                tagCompound.getCompoundTag("abilities").setTag("flySpeed", new NBTTagFloat(sp));

                SpeedUtil.setPlayerSpeed(player,type,MovementDirections.HORIZONTAL,sp);

            }


            player.capabilities.readCapabilitiesFromNBT(tagCompound);

            player.sendPlayerAbilities();
        } else {
            MpSpeed.kNetwork.sendTo(receiver.getPlayer(), MPSClientPackets.SET_SLIDER,perm.getMetadata().get(type.name().toLowerCase()+"_horizontal").asDouble());
        }

    }

}
