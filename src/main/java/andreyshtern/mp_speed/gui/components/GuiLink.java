package andreyshtern.mp_speed.gui.components;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiConfirmOpenLink;
import net.minecraft.client.gui.GuiScreen;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class GuiLink {
    private String text;
    private String link;
    private int x;
    private int y;
    private int color;
    private FontRenderer fontRenderer;
    public GuiLink(String text, String link, int x, int y, int color){
        this.text = text;
        this.link = link;
        this.x = x;
        this.y = y;
        this.color = color;
    }
    public void drawLink(FontRenderer fontRenderer){
        fontRenderer.drawString(text, x,y,color,false);

        if(this.fontRenderer == null){
            this.fontRenderer = fontRenderer;
        }
    }
    public void checkLinkClick(int x, int y, int button) {
        if(button != 0){ return; }
        if(ifInBounds(x,this.x,this.x+fontRenderer.getStringWidth(text))&&ifInBounds(y,this.y,this.y+fontRenderer.FONT_HEIGHT)){
            try {
                if (Desktop.isDesktopSupported()) {
                    // Windows
                    Desktop.getDesktop().browse(new URI(link));
                } else {
                    // Linux
                    Runtime runtime = Runtime.getRuntime();
                    runtime.exec("/usr/bin/firefox -new-window " + link);
                }
            } catch (Exception e){
                e.printStackTrace();
            }
        }

    }
    private boolean ifInBounds(int value, int minValue, int maxValue){
        return value >= minValue && value <= maxValue;
    }
}
