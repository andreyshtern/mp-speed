package andreyshtern.mp_speed.gui.components;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import andreyshtern.mp_speed.MpSpeed;
import andreyshtern.mp_speed.structs.ButtonDirections;
import andreyshtern.mp_speed.structs.MovementDirections;


public class ButtonDirection extends GuiButton {
    protected static final ResourceLocation buttonTextures = new ResourceLocation(MpSpeed.MODID,"textures/gui/arrows.png");
    private MovementDirections dir;
    private ButtonDirections bdir;
    public ButtonDirection(int id, int x, int y, int w, int h, String text, MovementDirections dir, ButtonDirections bdir)
    {
        super(id, x, y, w, h, text);
        this.dir = dir;
        this.bdir = bdir;
    }

    public boolean isDisabled(){
        return super.enabled;
    }


    public void drawCenteredString(FontRenderer p_73732_1_, String p_73732_2_, int p_73732_3_, int p_73732_4_, int p_73732_5_)
    {
        p_73732_1_.drawString(p_73732_2_, p_73732_3_ - p_73732_1_.getStringWidth(p_73732_2_) / 2, p_73732_4_, p_73732_5_);
    }
    public void drawButton(Minecraft p_146112_1_, int p_146112_2_, int p_146112_3_) {
        if (this.visible) {
            FontRenderer fontrenderer = p_146112_1_.fontRenderer;
            p_146112_1_.getTextureManager().bindTexture(buttonTextures);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.field_146123_n = p_146112_2_ >= this.xPosition && p_146112_3_ >= this.yPosition && p_146112_2_ < this.xPosition + this.width && p_146112_3_ < this.yPosition + this.height;
            int k = this.getHoverState(this.field_146123_n);
            GL11.glEnable(3042);
            OpenGlHelper.glBlendFunc(770, 771, 1, 0);
            GL11.glBlendFunc(770, 771);
            int[] offsets = getTextureOffsets();
            this.drawTexturedModalRect(this.xPosition, this.yPosition,offsets[0],0,offsets[1],offsets[2]);
            this.mouseDragged(p_146112_1_, p_146112_2_, p_146112_3_);


        }

    }
    private int[] getTextureOffsets(){
        int startPixelX = 0;

        if(dir == MovementDirections.HORIZONTAL){
            startPixelX += 56;
            if(bdir == ButtonDirections.LEFT){
                startPixelX += 8;
            }
            if(isDisabled()){
                startPixelX += 16;
            }
            return new int[]{startPixelX,8,14};
        } else {
            if(bdir == ButtonDirections.DOWN){
                startPixelX += 14;
            }
            if(isDisabled()){
                startPixelX += 28;
            }
            return new int[]{startPixelX,14,8};
        }


    }
}
