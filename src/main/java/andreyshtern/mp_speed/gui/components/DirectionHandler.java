package andreyshtern.mp_speed.gui.components;

import andreyshtern.mp_speed.gui.GuiSpeed;
import andreyshtern.mp_speed.structs.MovementDirections;
import andreyshtern.mp_speed.utils.DirectionUtils;


import java.util.HashMap;

public class DirectionHandler {
    private static HashMap<Integer,ButtonDirection> vertical_b = new HashMap<Integer,ButtonDirection>();
    private static HashMap<Integer,ButtonDirection> horizontal_b = new HashMap<Integer,ButtonDirection>();
    private static MovementDirections currentDir = MovementDirections.HORIZONTAL;
    private static boolean isInitialized = false;



    public static void init(ButtonDirection h1, ButtonDirection h2, ButtonDirection v1, ButtonDirection v2){
        horizontal_b.put(h1.id,h1);
        horizontal_b.put(h2.id,h2);
        vertical_b.put(v1.id,v1);
        vertical_b.put(v2.id,v2);
        if(!isInitialized){

            setCurrentDir(MovementDirections.HORIZONTAL);
        } else {
            setDirections(currentDir);
        }

        isInitialized = true;
    }
    private static void setDirections(MovementDirections dir){
        if(dir == MovementDirections.HORIZONTAL){
            for(ButtonDirection b : horizontal_b.values()){
                b.enabled = false;

            }
            for(ButtonDirection b : vertical_b.values()){
                b.enabled = true;

            }
        } else {
            for(ButtonDirection b : vertical_b.values()){
                b.enabled = false;
            }
            for(ButtonDirection b : horizontal_b.values()){
                b.enabled = true;
            }
        }
    }
    public static void setCurrentDir(MovementDirections dir){
        currentDir = dir;
        setDirections(dir);
    }
    public static MovementDirections getCurrentDir(){
        return currentDir;
    }
    public static void checkDirections(GuiSpeed speed, int id){
        for(int b : horizontal_b.keySet()){
            if(b == id){
                setCurrentDir(MovementDirections.HORIZONTAL);
                speed.changeScreen(GuiSpeed.getCurrentType(),DirectionUtils.getOppositeDirection(getCurrentDir()));
                return;
            }

        }
        for(int b : vertical_b.keySet()){
            if(b == id){
                setCurrentDir(MovementDirections.VERTICAL);
                speed.changeScreen(GuiSpeed.getCurrentType(),DirectionUtils.getOppositeDirection(getCurrentDir()));
                return;
            }

        }
    }


}
