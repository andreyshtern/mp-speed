package andreyshtern.mp_speed.gui.components;

import cpw.mods.fml.client.config.GuiSlider;
import cpw.mods.fml.client.config.GuiUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import andreyshtern.mp_speed.MpSpeed;
import org.lwjgl.opengl.GL11;

public class SliderSpeed extends GuiSlider {
    private static final ResourceLocation buttonTextures = new ResourceLocation(MpSpeed.MODID,"textures/gui/widgets.png");
    private static final int packedFGColour = 0x727272;
    private int restrictValue = 1;
    public SliderSpeed(int id, int xPos, int yPos, int width, int height, String prefix, String suf, double minVal, double maxVal, double currentVal, boolean showDec, boolean drawStr, int restrictValue)
    {
        super(id, xPos, yPos, width, height, prefix, suf, minVal, maxVal, currentVal, showDec, drawStr, null);
        this.restrictValue = restrictValue;


    }
    public SliderSpeed(int id, int xPos, int yPos, String displayStr, double minVal, double maxVal, double currentVal, ISlider par)
    {
        super(id, xPos, yPos, 150, 20, displayStr, "", minVal, maxVal, currentVal, true, true, par);

    }
    public String getName(){
        return super.displayString;
    }
    public void setName(String name){
        super.displayString = name;
    }
    public int getRestrictValue(){
        return restrictValue;
    }
    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY)
    {
        if (this.visible)
        {
            if(getValue()<=1)
                setValue(1);

            this.field_146123_n = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
            int k = this.getHoverState(this.field_146123_n);
            GuiUtils.drawContinuousTexturedBox(buttonTextures, this.xPosition, this.yPosition, 0, 46 + k * 20, this.width, this.height, 200, 20, 2, 3, 2, 2, this.zLevel);
            this.mouseDragged(mc, mouseX, mouseY);
            int color = 14737632;

            if (packedFGColour != 0)
            {
                color = packedFGColour;
            }
            else if (!this.enabled)
            {
                color = 10526880;
            }
            else if (this.field_146123_n)
            {
                color = 16777120;
            }

            String buttonText = this.displayString;
            int strWidth = mc.fontRenderer.getStringWidth(buttonText);
            int ellipsisWidth = mc.fontRenderer.getStringWidth("...");

            if (strWidth > width - 6 && strWidth > ellipsisWidth)
                buttonText = mc.fontRenderer.trimStringToWidth(buttonText, width - 6 - ellipsisWidth).trim() + "...";

            if(restrictValue != -1 && this.enabled && restrictValue != this.maxValue){
                int cellSize = this.width / (int) this.maxValue;
                int rectX = this.xPosition + cellSize * restrictValue;
                int rectW = this.xPosition + this.width;

                drawRect(rectX+cellSize/4,this.yPosition,rectW,this.yPosition+this.height,0xFF727272);
            }
            if(!this.enabled){
                drawRect(this.xPosition,this.yPosition,this.xPosition+this.width,this.yPosition+this.height,0xFF727272);
                drawCenteredString(mc.fontRenderer, I18n.format("mp_speed.gui.unavailable"), this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, 0xFFFFFF);

            } else {
                //Draws text
                drawCenteredString(mc.fontRenderer, buttonText, this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2 + 1, 0xFFFFFF);
                drawCenteredString(mc.fontRenderer, buttonText, this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, 0);
                //Draws Slider values
                drawCenteredString(mc.fontRenderer, "1", this.xPosition, this.yPosition-mc.fontRenderer.FONT_HEIGHT, 0);
                drawCenteredString(mc.fontRenderer, "10", this.xPosition+this.width, this.yPosition-mc.fontRenderer.FONT_HEIGHT, 0);

            }



        }
    }
    @Override
    protected void mouseDragged(Minecraft par1Minecraft, int mouseX, int mouseY) {
        if(!visible)
            return;

//        System.out.println("maxValue: "+maxValue);
//        System.out.println("cellSize: "+cellSize);
//        System.out.println("mouseX: "+mouseX);
//        System.out.println("xPos: "+xPosition);
        if(restrictValue != 1) {
            if (!isLargerThanMax(mouseX)) {
                if (this.dragging) {
                    this.sliderValue = (mouseX - (this.xPosition + 4)) / (float) (this.width - 8);

                    updateSlider();
                }
            } else if (this.dragging) {
                int cellSize = this.width / (int) this.maxValue;

//            this.sliderValue = (((restrictValue*cellSize-66.01f) - (this.xPosition + 4)) / (float)(this.width - 8))+(restrictValue/2f);
                //System.out.println(restrictValue);

                this.setValue(restrictValue + 0.5f);

                if (getValueInt() >= 10)
                    setValue(10);
                //updateSlider();
            }
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        }
        this.drawTexturedModalRect(this.xPosition + (int)(this.sliderValue * (float)(this.width - 8)), this.yPosition, 0, 66, 4, 20);
        this.drawTexturedModalRect(this.xPosition + (int)(this.sliderValue * (float)(this.width - 8)) + 4, this.yPosition, 196, 66, 4, 20);


    }
    private boolean isLargerThanMax(int mouseX){

        return !(restrictValue == -1 || (mouseX < xPosition + getMaxAvailableValue()));
    }
    private int getMaxAvailableValue(){

        int cellSize = this.width / (int) this.maxValue;
        int maxValue = restrictValue == -1 ? (int)this.maxValue * cellSize : cellSize * restrictValue;
        return maxValue;
    }
    @Override
    public boolean mousePressed(Minecraft par1Minecraft, int mouseX, int mouseY)
    {
        if(restrictValue == 1)
            return false;
        boolean isInCtrl = this.enabled && this.visible && mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
        if(isInCtrl && !isLargerThanMax(mouseX))
            return super.mousePressed(par1Minecraft,mouseX,mouseY);
        return false;
    }
    public void drawCenteredString(FontRenderer p_73732_1_, String p_73732_2_, int p_73732_3_, int p_73732_4_, int p_73732_5_)
    {
        p_73732_1_.drawString(p_73732_2_, p_73732_3_ - p_73732_1_.getStringWidth(p_73732_2_) / 2, p_73732_4_, p_73732_5_);
    }
    public int getValueInt()
    {
        return (int)Math.floor(sliderValue * (maxValue - minValue) + minValue);
    }
}
