package andreyshtern.mp_speed.gui.components;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import andreyshtern.mp_speed.MpSpeed;
import andreyshtern.mp_speed.structs.MovementTypes;


public class ButtonType extends GuiButton {
    protected static final ResourceLocation buttonTextures = new ResourceLocation(MpSpeed.MODID,"textures/gui/buttons.png");
    private MovementTypes type;
    public ButtonType(MovementTypes type, int id, int x, int y, String text)
    {
        super(id, x, y, 50, 20, text);
        this.type = type;
    }
    public void tryToDisable(MovementTypes type){
        if(this.type == type){
            super.enabled = false;
        } else {
            super.enabled = true;
        }

    }
    public boolean isDisabled(){
        return super.enabled;
    }
    public MovementTypes getType(){
        return this.type;
    }

    public void drawButton(Minecraft p_146112_1_, int p_146112_2_, int p_146112_3_)
    {
        if (this.visible)
        {
            FontRenderer fontrenderer = p_146112_1_.fontRenderer;
            p_146112_1_.getTextureManager().bindTexture(buttonTextures);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.field_146123_n = p_146112_2_ >= this.xPosition && p_146112_3_ >= this.yPosition && p_146112_2_ < this.xPosition + this.width && p_146112_3_ < this.yPosition + this.height;
            int k = this.getHoverState(this.field_146123_n);
            GL11.glEnable(GL11.GL_BLEND);
            OpenGlHelper.glBlendFunc(770, 771, 1, 0);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
            this.drawTexturedModalRect(this.xPosition, this.yPosition, 0, 46 + k * 20, this.width / 2, this.height);
            this.drawTexturedModalRect(this.xPosition + this.width / 2, this.yPosition, 200 - this.width / 2, 46 + k * 20, this.width / 2, this.height);
            this.mouseDragged(p_146112_1_, p_146112_2_, p_146112_3_);


            if(k==2) {
                this.drawCenteredString(fontrenderer, this.displayString, this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, 0xFFFFFF);

            } else if(super.enabled){
                this.drawCenteredString(fontrenderer, this.displayString, this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, 0x5B5B5B);
            } else {
                this.drawCenteredString(fontrenderer, this.displayString, this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, 0xFFFFFF);

            }
        }
    }
    public void drawCenteredString(FontRenderer p_73732_1_, String p_73732_2_, int p_73732_3_, int p_73732_4_, int p_73732_5_)
    {
        p_73732_1_.drawString(p_73732_2_, p_73732_3_ - p_73732_1_.getStringWidth(p_73732_2_) / 2, p_73732_4_, p_73732_5_);
    }
    public static MovementTypes getCurrentState(ButtonType... b1){
        for(ButtonType b : b1){
            if(!b.isDisabled()){
                return b.type;
            }
        }
        //Default value
        return MovementTypes.WALK;
    }
}
