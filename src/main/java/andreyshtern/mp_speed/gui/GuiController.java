package andreyshtern.mp_speed.gui;

import net.minecraft.entity.player.EntityPlayer;
import andreyshtern.mp_speed.structs.MaxSpeeds;

public class GuiController {
    private static String nick = null;
    private static MaxSpeeds speeds = null;
    private static String linkUrl;
    private static boolean isOp;
    private static EntityPlayer player;

    public static EntityPlayer getPlayer() {
        return player;
    }

    public static void setPlayer(EntityPlayer playerToSet) {
        player = playerToSet;
    }

    public static void setSpeeds(MaxSpeeds speeds) {
        GuiController.speeds = speeds;
    }

    public static MaxSpeeds getSpeeds() {
        return speeds;
    }

    public static void setNick(String nick) {
        GuiController.nick = nick;
    }

    public static String getNick() {
        return nick;
    }

    public static String getLinkUrl() {
        return linkUrl;
    }

    public static void setLinkUrl(String linkUrl) {
        GuiController.linkUrl = linkUrl;
    }

    public static boolean isOp() {
        return isOp;
    }

    public static void setIsOp(boolean isOp) {
        GuiController.isOp = isOp;
    }
}
