package andreyshtern.mp_speed.gui;

import andreyshtern.mp_speed.gui.components.*;
import andreyshtern.mp_speed.props.PlayerSpeedProps;
import andreyshtern.mp_speed.structs.*;
import cpw.mods.fml.client.config.GuiSlider;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.*;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import andreyshtern.mp_speed.MpSpeed;
import andreyshtern.mp_speed.entity.FakePlayerClient;
import andreyshtern.mp_speed.gui.components.*;


import andreyshtern.mp_speed.structs.*;
import andreyshtern.mp_speed.utils.SpeedUtil;

import java.util.ArrayList;
import java.util.List;

public class GuiSpeed extends GuiScreen {
    public static SliderSpeed mSlider;
    private EntityPlayer player;
    private static DirectionHandler dirHandler;
    private static MaxSpeeds maxSpeed;

    private GuiLink link;
    private List<SpeedToSend> valuesToSend = new ArrayList<SpeedToSend>();
    private List<SpeedToSend> criticalValuesToSend = new ArrayList<SpeedToSend>();
    private FakePlayerClient fakePlayer;

    int guiLeft, guiTop;
    List<GuiButton> buttonTypeList = new ArrayList<GuiButton>();
    private static MovementTypes currentType = MovementTypes.WALK;

    public static MovementTypes getCurrentType() {
        return currentType;
    }
    private ButtonDirection h1,h2,v1,v2;

    public static MaxSpeeds getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.buttonList.clear();
        if(GuiController.getNick() != null){
            player = GuiController.getPlayer();
            maxSpeed = GuiController.getSpeeds();
            if(player != null){
                fakePlayer = new FakePlayerClient(player.worldObj,player.getGameProfile());

            }
        }

        guiLeft = (this.width - 470) / 2;
        guiTop = (this.height - 200) / 2;

        v1 = new ButtonDirection(4, guiLeft + 70,guiTop+25, 28, 16,"", MovementDirections.VERTICAL, ButtonDirections.UP);
        v2 = new ButtonDirection(5, guiLeft + 70,guiTop+180, 28, 16,"", MovementDirections.VERTICAL, ButtonDirections.DOWN);
        h1 = new ButtonDirection(6, guiLeft + 23,guiTop+90, 16, 28,"",MovementDirections.HORIZONTAL, ButtonDirections.LEFT);
        h2 = new ButtonDirection(7, guiLeft + 120, guiTop+90,16, 28, "",MovementDirections.HORIZONTAL, ButtonDirections.RIGHT);
        this.buttonList.add(h1);
        this.buttonList.add(h2);

        this.buttonList.add(v1);
        this.buttonList.add(v2);
        CustomButton done = new CustomButton(8, guiLeft+100, guiTop + 170,I18n.format("mp_speed.gui.done"),145);
        CustomButton cancel = new CustomButton(9, guiLeft+100, guiTop + 170,I18n.format("mp_speed.gui.cancel"),145);
        done.xPosition = calculateCenter(done) - 75;
        cancel.xPosition = calculateCenter(cancel) + 75;
        done.enabled = player != null;
        this.buttonList.add(done);
        this.buttonList.add(cancel);
        this.buttonList.add(new CustomButton(10,guiLeft+395, guiTop+5, I18n.format("mp_speed.gui.reset"), 60));

        link = new GuiLink(I18n.format("mp_speed.gui.shop_ad.link"),GuiController.getLinkUrl(),calculateCenter("mp_speed.gui.shop_ad.link")+(fontRendererObj.getStringWidth(I18n.format("mp_speed.gui.shop_ad.line2"))+fontRendererObj.getStringWidth(" "))/2,guiTop+143+fontRendererObj.FONT_HEIGHT,0x1A9DE1);
        DirectionHandler.init(h1,h2,v1,v2);
        int restrictValue = (int) getValue();
        if(maxSpeed != null){
            restrictValue = maxSpeed.getMaxValue(DirectionHandler.getCurrentDir(),currentType);
        }
        this.buttonList.add(mSlider = new SliderSpeed(0,guiLeft+200,guiTop+70, fontRendererObj.getStringWidth(I18n.format("mp_speed.gui.speed_control"))*3,20,"","",1,10,initValue(),false,true,restrictValue));

        //mSlider.xPosition = guiLeft+240-mSlider.width/2;

        if(maxSpeed!=null&&!GuiController.isOp()&&mSlider.getValueInt()>maxSpeed.getMaxValue(DirectionHandler.getCurrentDir(),currentType)){
            mSlider.setValue(maxSpeed.getMaxValue(DirectionHandler.getCurrentDir(),currentType));
            mSlider.updateSlider();
            criticalValuesToSend.add(new SpeedToSend(currentType, DirectionHandler.getCurrentDir(),mSlider.getValueInt()));
        }
        if((restrictValue == -1 && mSlider.getValueInt() == mSlider.maxValue)||
                (mSlider.getValueInt() == restrictValue)){
            mSlider.setValue(restrictValue);
        } else if(mSlider.getValueInt() == 1){
            mSlider.setValue(1);
        } else {
            mSlider.setValue(mSlider.getValueInt()+0.5f);
        }

        ButtonType bt1,bt2,bt3;
        bt1 = new ButtonType(MovementTypes.FLY,1,guiLeft,guiTop+110,I18n.format("mp_speed.gui.speed.type.fly"));
        bt2 = new ButtonType(MovementTypes.SWIM,2,guiLeft,guiTop+110,I18n.format("mp_speed.gui.speed.type.swim"));
        bt3 = new ButtonType(MovementTypes.WALK,3,guiLeft,guiTop+110,I18n.format("mp_speed.gui.speed.type.walk"));
        bt1.xPosition = calculateCenter(bt1)-48;
        bt2.xPosition = calculateCenter(bt2);
        bt3.xPosition = calculateCenter(bt3)+48;
        buttonTypeList.add(bt1);
        buttonTypeList.add(bt2);
        buttonTypeList.add(bt3);

        this.buttonList.add(bt1);
        this.buttonList.add(bt2);
        this.buttonList.add(bt3);

        checkDisable(currentType);
        currentType = ButtonType.getCurrentState(bt1,bt2,bt3);

    }
    private int calculateCenter(String translationKey){
        return guiLeft+265+fontRendererObj.getStringWidth(I18n.format("mp_speed.gui.speed_control"))/2-fontRendererObj.getStringWidth(I18n.format(translationKey))/2;
    }
    private int calculateCenter(GuiButton button){
        return guiLeft+265+fontRendererObj.getStringWidth(I18n.format("mp_speed.gui.speed_control"))/2-button.width/2;
    }
    private int calculateCenter(int width){
        return guiLeft+265+fontRendererObj.getStringWidth(I18n.format("mp_speed.gui.speed_control"))/2-width/2;
    }

    private float initValue(){
        if(player == null || maxSpeed == null)
            return 1;
        if(mSlider.getValue() != 1){

            return (float) mSlider.getValue();
        }



        return getValue();
    }
    private float getValue(){
        if(player == null || maxSpeed == null){
            return 1;
        }

        float speed;
        SpeedToSend valueFromList = SpeedToSend.getValueFromListOfChanges(valuesToSend, DirectionHandler.getCurrentDir(),currentType);
        if(valueFromList!=null){

            speed = valueFromList.getValue();
        } else {
            speed = SpeedUtil.getPlayerSpeed(player,currentType,DirectionHandler.getCurrentDir());

            if(DirectionHandler.getCurrentDir()==MovementDirections.HORIZONTAL){
                if(currentType == MovementTypes.FLY){
                    return player.capabilities.getFlySpeed() == 0.05f ? player.capabilities.getFlySpeed() / 0.05f : player.capabilities.getFlySpeed() / 0.03f;

                } else if (currentType == MovementTypes.WALK) {
                    try {

                        return SpeedUtil.getPlayerSpeed(player,currentType,DirectionHandler.getCurrentDir());

                    } catch (NullPointerException e){
                        return 1;
                    }
                }


            } else if(DirectionHandler.getCurrentDir() == MovementDirections.VERTICAL){
                return speed + 1;
            }

        }

        return speed;

    }

    private void checkDisable(MovementTypes type){

        for(GuiButton b: buttonTypeList){
            ((ButtonType)b).tryToDisable(type);


        }
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        MovementTypes typeToSet;

        DirectionHandler.checkDirections(this,button.id);
        switch (button.id){

            case 1:
                //Fly Type Button
                typeToSet = MovementTypes.FLY;
                checkDisable(typeToSet);
                changeScreen(typeToSet, DirectionHandler.getCurrentDir());
                break;
            case 2:
                //Swim Type Button
                typeToSet = MovementTypes.SWIM;
                checkDisable(typeToSet);
                changeScreen(typeToSet, DirectionHandler.getCurrentDir());
                break;
            case 3:
                //Walk Type Button
                typeToSet = MovementTypes.WALK;
                checkDisable(typeToSet);
                changeScreen(typeToSet, DirectionHandler.getCurrentDir());
                break;
            case 8:
                //Done Button
                SpeedToSend.overwriteValue(valuesToSend,new SpeedToSend(currentType,DirectionHandler.getCurrentDir(),mSlider.getValueInt()));

                closeScreen();
                sendValues(valuesToSend);
                break;
            case 9:
                //Cancel button
                closeScreen();
                break;
            case 10:
                //Reset button
                for(MovementDirections dir : MovementDirections.values()){
                    for(MovementTypes type : MovementTypes.values()){
                        SpeedToSend.overwriteValue(valuesToSend,new SpeedToSend(type,dir,1));
                        mSlider.setValue(1);
                    }
                }
        }



    }

    private void sendValues(List<SpeedToSend> list){
        for(SpeedToSend send : list){
            int value = send.getValue();
            if(send.getDir() == MovementDirections.VERTICAL)
                value -= 1;
            SpeedUtil.setPlayerSpeed(player,send.getType(),send.getDir(),value);

        }
        list.clear();
    }
    public void changeScreen(MovementTypes type, MovementDirections dir){

        int valueToSet = mSlider.getValueInt();

        SpeedToSend.overwriteValue(valuesToSend,new SpeedToSend(currentType,dir,valueToSet));
        currentType = type;
        changeScreen();
    }

    public void changeScreen(){

        mSlider.setValue(getValue());
        mSlider.updateSlider();

    }
    @Override
    public void mouseClicked(int x, int y, int button)
    {
        super.mouseClicked(x, y, button);

        link.checkLinkClick(x, y, button);

    }
    private void closeScreen(){
        if(!criticalValuesToSend.isEmpty())
            sendValues(criticalValuesToSend);
        Minecraft.getMinecraft().thePlayer.closeScreen();
    }
    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        if(Minecraft.getMinecraft().thePlayer.isDead){
            closeScreen();
            return;
        }
        ScaledResolution sr = new ScaledResolution(Minecraft.getMinecraft(), Minecraft.getMinecraft().displayWidth, Minecraft.getMinecraft().displayWidth);

        ResourceLocation background = new ResourceLocation(MpSpeed.MODID, "textures/gui/bg.png");
        Minecraft.getMinecraft().renderEngine.bindTexture(background);

        this.drawTexturedModalRect(guiLeft, guiTop, 100, 100, 460, 200);
        if(player != null)
            fontRendererObj.drawString(player.getDisplayName(),v1.xPosition+v1.width/4 - fontRendererObj.getStringWidth(player.getDisplayName()) / 2,guiTop+10,0,false);
        fontRendererObj.drawString(I18n.format("mp_speed.gui.speed_control"),guiLeft+265,guiTop+10,0,false);

        if(DirectionHandler.getCurrentDir()== MovementDirections.HORIZONTAL){
            mSlider.setName(I18n.format("mp_speed.gui.speed.horizontal")+": "+mSlider.getValueInt());


        } else {
            mSlider.setName(I18n.format("mp_speed.gui.speed.vertical")+": "+mSlider.getValueInt());


        }
        fontRendererObj.drawString(I18n.format("mp_speed.gui.shop_ad.line1"),
                calculateCenter("mp_speed.gui.shop_ad.line1"),guiTop+140,0x5E5E5E,false);

            fontRendererObj.drawString(I18n.format("mp_speed.gui.shop_ad.line2"),
                    calculateCenter(fontRendererObj.getStringWidth(I18n.format("mp_speed.gui.shop_ad.line2")+" "+I18n.format("mp_speed.gui.shop_ad.link"))),guiTop+143+fontRendererObj.FONT_HEIGHT,0x5E5E5E,false);
        link.drawLink(fontRendererObj);
        mSlider.enabled = maxSpeed.getMaxValue(DirectionHandler.getCurrentDir(), currentType) != 1;
        mSlider.enabled = !(DirectionHandler.getCurrentDir()==MovementDirections.VERTICAL &&
                currentType == MovementTypes.WALK);

        if(!mSlider.enabled)
            mSlider.setValue(1);

        renderPlayerGUI(guiLeft + 77, guiTop + 65, 60, fakePlayer, partialTicks);

        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }
    float rotY = 0;
    //50% Inherited from Vanilla MC
    public void renderPlayerGUI(int x, int y, int scale, FakePlayerClient entity, float partialTicks)
    {


        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        GL11.glPushMatrix();
        GL11.glTranslatef((float)x, (float)y, 50.0F);
        GL11.glScalef((float)(-scale), (float)scale, (float)scale);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);

        //GL11.glRotatef(135.0F, 0.0F, 1.0F, 0.0F);
        RenderHelper.enableStandardItemLighting();

        GL11.glRotatef(-rotY, 0.0F, 1.0F, 0.0F);


        rotY = (rotY + partialTicks * 2);
        RenderManager.instance.playerViewY = 180.0F;
        RenderManager.instance.renderEntityWithPosYaw(entity, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);


        GL11.glPopMatrix();
        RenderHelper.disableStandardItemLighting();
        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
        OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
    }

}

