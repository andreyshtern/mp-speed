package andreyshtern.mp_speed.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import keelfy.mppermit.permission.Metadata;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import andreyshtern.mp_speed.MpSpeed;
import andreyshtern.mp_speed.network.MPSClientPackets;
import andreyshtern.mp_speed.perm.Permissions;
import andreyshtern.mp_speed.props.PlayerSpeedProps;
import andreyshtern.mp_speed.structs.MovementDirections;
import andreyshtern.mp_speed.structs.MovementTypes;
import andreyshtern.mp_speed.structs.NBTUtils;
import andreyshtern.mp_speed.structs.Speeds;
import andreyshtern.mp_speed.utils.SpeedUtil;

import java.util.HashMap;
import java.util.Map;

public class ServerEventHandler {
    private static HashMap<EntityPlayer,Integer> timers = new HashMap<EntityPlayer, Integer>();

    @SubscribeEvent
    public void playerJoined(PlayerEvent.PlayerLoggedInEvent e){
        if(!MpSpeed.server)
            return;

        timers.put(e.player,0);

        Map<String, Metadata> meta = Permissions.SPEED.getAllMetadata((EntityPlayerMP) e.player,false);
        if(meta == null)
            return;
        for(MovementDirections m : MovementDirections.values()){
            for(MovementTypes types : MovementTypes.values()){
                if(meta.containsKey(types.name().toLowerCase()+"_"+m.name().toLowerCase())){
                    int max = 1;
                    try{
                        max = meta.get(types.name().toLowerCase()+"_"+m.name().toLowerCase()).asInt();
                    } catch (NullPointerException ignored){ }

                    if(m == MovementDirections.HORIZONTAL){
                        if(PlayerSpeedProps.get(e.player).getSaved().getHCaps(types)>max){
                            Speeds toCorrect = PlayerSpeedProps.get(e.player).getSaved();
                            toCorrect.setHCaps(types,max);
                            PlayerSpeedProps.get(e.player).setSaved(toCorrect);
                        }
                    } else {
                        if(PlayerSpeedProps.get(e.player).getSaved().getSpeed(types)>max){
                            Speeds toCorrect = PlayerSpeedProps.get(e.player).getSaved();
                            toCorrect.setSpeed(types,max);
                            PlayerSpeedProps.get(e.player).setSaved(toCorrect);

                        }
                    }
                } else {
                    if(m == MovementDirections.HORIZONTAL){

                        Speeds toCorrect = PlayerSpeedProps.get(e.player).getSaved();
                        toCorrect.setHCaps(types,1);
                        PlayerSpeedProps.get(e.player).setSaved(toCorrect);

                    } else {

                        Speeds toCorrect = PlayerSpeedProps.get(e.player).getSaved();
                        toCorrect.setSpeed(types,1);
                        PlayerSpeedProps.get(e.player).setSaved(toCorrect);


                    }
                }


            }
        }



        MpSpeed.kNetwork.sendTo((EntityPlayerMP) e.player, MPSClientPackets.SEND_SPEED, NBTUtils.getNBTFromSpeeds(SpeedUtil.getSpeeds(e.player)));

    }
    @SubscribeEvent
    public void playerUpdate(TickEvent.ServerTickEvent e){
        if(!MpSpeed.server)
            return;

        for(EntityPlayer t : timers.keySet()){
            if(timers.get(t) == 0){
                MpSpeed.kNetwork.sendTo((EntityPlayerMP) t, MPSClientPackets.SEND_SPEED, NBTUtils.getNBTFromSpeeds(SpeedUtil.getSpeeds(t)));

                t.sendPlayerAbilities();

            } else {
                timers.put(t,timers.get(t) - 1);
            }

        }
    }

    @SubscribeEvent
    public void playerTickUpdate(TickEvent.PlayerTickEvent event){
        for(MovementTypes type : MovementTypes.values()){

            if(PlayerSpeedProps.get(event.player).getSaved().getHCaps(type)/10-1>SpeedUtil.getPlayerSpeed(event.player,type,MovementDirections.HORIZONTAL)){
                SpeedUtil.setPlayerSpeed(event.player, type, MovementDirections.HORIZONTAL, PlayerSpeedProps.get(event.player).getSaved().getSpeed(type));
                MpSpeed.kNetwork.sendTo((EntityPlayerMP) event.player,MPSClientPackets.SET_SPEED_REMOTE,
                        MovementDirections.HORIZONTAL.name(), type.name(), PlayerSpeedProps.get(event.player).getSaved().getHCaps(type));
            }


        }
    }


}
