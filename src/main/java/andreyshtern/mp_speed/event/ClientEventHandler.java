package andreyshtern.mp_speed.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import keelfy.klibrary.utils.KVector;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialLiquid;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.player.EntityPlayer;
import andreyshtern.mp_speed.MpSpeed;
import andreyshtern.mp_speed.network.MPSServerPackets;
import andreyshtern.mp_speed.structs.MovementDirections;
import andreyshtern.mp_speed.structs.MovementTypes;
import andreyshtern.mp_speed.structs.MoverUtils;
import andreyshtern.mp_speed.utils.PlayerUtils;
import andreyshtern.mp_speed.utils.SpeedUtil;
import net.minecraft.init.Items;
import net.minecraftforge.client.event.EntityViewRenderEvent;
import net.minecraftforge.client.event.FOVUpdateEvent;

public class ClientEventHandler {
    @SubscribeEvent
    public void playerTick(TickEvent.PlayerTickEvent e) {
        if (e.phase == TickEvent.Phase.END)
            return;
        EntityPlayerSP player;
        try{
            player = (EntityPlayerSP) e.player;
        } catch (ClassCastException exc){
            return;
        }

        GameSettings keybinds = Minecraft.getMinecraft().gameSettings;
        MovementTypes type;

        if (player.capabilities.isFlying) {
            type = MovementTypes.FLY;

        } else if (player.isInWater()) {
            type = MovementTypes.SWIM;

        } else {

            return;
        }

        double yMovement = SpeedUtil.getPlayerSpeed(player, type, MovementDirections.VERTICAL);
        if (!(keybinds.keyBindJump.getIsKeyPressed() && keybinds.keyBindSneak.getIsKeyPressed())) {
            boolean jump = keybinds.keyBindJump.getIsKeyPressed();
            boolean sneak = keybinds.keyBindSneak.getIsKeyPressed();
            if (jump ^ sneak){
                int distance;
                double distanceToSubstract = 0;
                KVector playerPos = new KVector(player);

                if (type == MovementTypes.SWIM) {
                    if (sneak) {

                        distance = PlayerUtils.getBlocksDownBeforeAir(player.getEntityWorld(), player, (int) yMovement);
                    } else {
                        distance = PlayerUtils.getBlocksUpBeforeAir(player.getEntityWorld(), player, (int) yMovement);

                    }


                    if (distance == 1) {
                        return;
                    }
                    distanceToSubstract = distance == 0 ? 0 : (yMovement - distance);
                }
                if (sneak) {
                    yMovement = -yMovement;
                }
                if ((player.worldObj.getBlock(playerPos.getBlockX(), playerPos.getBlockY() + 1, playerPos.getBlockZ()).getMaterial() instanceof MaterialLiquid)
                        || type != MovementTypes.SWIM){
                    player.moveEntity(0, yMovement - distanceToSubstract, 0);
                    MpSpeed.kNetwork.sendToServer(MPSServerPackets.MOVE_ENTITY, 0, yMovement - distanceToSubstract, 0);
                }

            }


        }
        double motionXBackup = player.motionX;
        double motionZBackup = player.motionZ;
        if (type == MovementTypes.SWIM) {
            int speed = (int) SpeedUtil.getPlayerSpeed(player, type, MovementDirections.HORIZONTAL);
            if (keybinds.keyBindForward.getIsKeyPressed()) {
                MoverUtils.moveEntityWithHeadingOptimised(player, 0, 1, speed);

            } else if (keybinds.keyBindBack.getIsKeyPressed()) {
                MoverUtils.moveEntityWithHeadingOptimised(player, 0, -1, speed);
            }
            if (keybinds.keyBindRight.getIsKeyPressed()) {
                MoverUtils.moveEntityWithHeadingOptimised(player, -1, 0, speed);
            } else if (keybinds.keyBindLeft.getIsKeyPressed()) {
                MoverUtils.moveEntityWithHeadingOptimised(player, 1, 0, speed);
            }
        }
        player.motionX = motionXBackup;
        player.motionZ = motionZBackup;

    }

    @SubscribeEvent
    public void fovUpdate(FOVUpdateEvent event) {


        float f = 1.0F;

        if (event.entity.capabilities.isFlying)
        {
            f *= 1.1F;
        }

        IAttributeInstance iattributeinstance = event.entity.getEntityAttribute(SharedMonsterAttributes.movementSpeed);
        if(SpeedUtil.getPlayerSpeed(event.entity,MovementTypes.WALK,MovementDirections.HORIZONTAL)>1){
            if(SpeedUtil.getPlayerSpeed(event.entity,MovementTypes.WALK,MovementDirections.HORIZONTAL)>3){
                f += 0.3f;
            } else {
                f += SpeedUtil.getPlayerSpeed(event.entity,MovementTypes.WALK,MovementDirections.HORIZONTAL)*0.1f;
            }


        } else {
            f = (float)((double)f * ((iattributeinstance.getAttributeValue() / (double)event.entity.capabilities.getWalkSpeed() + 1.0D) / 2.0D));
        }




        if (event.entity.capabilities.getWalkSpeed() == 0.0F || Float.isNaN(f) || Float.isInfinite(f))
        {
            f = 1.0F;
        }

        if (event.entity.isUsingItem() && event.entity.getItemInUse().getItem() == Items.bow)
        {
            int i = event.entity.getItemInUseDuration();
            float f1 = (float)i / 20.0F;

            if (f1 > 1.0F)
            {
                f1 = 1.0F;
            }
            else
            {
                f1 *= f1;
            }

            f *= 1.0F - f1 * 0.15F;
        }
        event.newfov = f;
    }




}
