package andreyshtern.mp_speed.structs;

public enum ButtonDirections {
    UP, LEFT, RIGHT, DOWN;
}
