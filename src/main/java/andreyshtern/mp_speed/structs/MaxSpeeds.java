package andreyshtern.mp_speed.structs;

import keelfy.mppermit.permission.Metadata;
import net.minecraft.nbt.NBTTagCompound;

import java.util.HashMap;
import java.util.Map;

public class MaxSpeeds {
    public MaxSpeeds(){}
    public MaxSpeeds(boolean isOp){
        if(isOp){
            setMaxValues();
        }
    }
    private HashMap<MovementTypes,Integer> horizontal = new HashMap<MovementTypes, Integer>();
    private HashMap<MovementTypes,Integer> vertical = new HashMap<MovementTypes, Integer>();

    public static MaxSpeeds getMaxSpeedsFromMeta(Map<String, Metadata> meta){
        MaxSpeeds ret = new MaxSpeeds();
        for(String key : meta.keySet()){
            ret.setMaxValue(MovementTypes.valueOf(key.split("_")[0].toUpperCase()),
                    MovementDirections.valueOf(key.split("_")[1].toUpperCase()),
                        meta.get(key).asInt());

        }
        return ret;
    }
    public static NBTTagCompound maxSpeedsToNBT(MaxSpeeds max){
        NBTTagCompound ret = new NBTTagCompound();
        NBTTagCompound horizontal = new NBTTagCompound();
        NBTTagCompound vertical = new NBTTagCompound();
        for(MovementTypes type : max.horizontal.keySet()){
            horizontal.setInteger(type.name(),max.horizontal.get(type));
        }
        for(MovementTypes type : max.vertical.keySet()){
            vertical.setInteger(type.name(),max.vertical.get(type));
        }
        ret.setTag("horizontal",horizontal);
        ret.setTag("vertical", vertical);
        return ret;
    }
    public static MaxSpeeds NBTToMaxSpeeds(NBTTagCompound nbt){
        MaxSpeeds ret = new MaxSpeeds();
        NBTTagCompound horizontal = nbt.getCompoundTag("horizontal");
        NBTTagCompound vertical = nbt.getCompoundTag("vertical");
        for(MovementTypes type : MovementTypes.values()){
            ret.setMaxValue(type,MovementDirections.HORIZONTAL,horizontal.getInteger(type.name()));
            ret.setMaxValue(type,MovementDirections.VERTICAL,vertical.getInteger(type.name()));
        }
        return ret;
    }
    public void setMaxValue(MovementTypes type, MovementDirections dir, int value){
        if(dir == MovementDirections.HORIZONTAL){
            horizontal.put(type,value);

        } else {
            vertical.put(type,value);

        }
    }
    public int needToRestrict(MovementDirections dir, MovementTypes type, int value){
        if(getMapFromDir(dir).get(type)<value){
            return getMapFromDir(dir).get(type);
        }
        return -1;
    }
    public int getMaxValue(MovementDirections dir, MovementTypes type){
        return getMapFromDir(dir).get(type);
    }
    private HashMap<MovementTypes,Integer> getMapFromDir(MovementDirections dir){
        switch (dir){
            case VERTICAL:
                return vertical;
            case HORIZONTAL:
                return horizontal;
        }
        return null;
    }
    public void setMaxValues(){
        for(MovementTypes t : MovementTypes.values()){
            horizontal.put(t,10);
            vertical.put(t,10);
        }
    }
    public void setDefaults(){
        for(MovementTypes t : MovementTypes.values()){
            horizontal.put(t,1);
            vertical.put(t,1);
        }
    }
}
