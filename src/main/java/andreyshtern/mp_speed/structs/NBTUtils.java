package andreyshtern.mp_speed.structs;

import net.minecraft.nbt.NBTTagCompound;

public class NBTUtils {
    public static NBTTagCompound getNBTFromSpeeds(Speeds s){
        NBTTagCompound ret = new NBTTagCompound();
        for(MovementTypes t : MovementTypes.values()){
            ret.setFloat(t.name(),s.getSpeed(t));
        }
        ret.setFloat("flySpeedHCap",s.getHCaps(MovementTypes.FLY));
        ret.setFloat("walkSpeedHCap",s.getHCaps(MovementTypes.WALK));
        ret.setFloat("swimSpeedHCap",s.getHCaps(MovementTypes.SWIM));
        return ret;
    }
    public static Speeds getSpeedsFromNBT(NBTTagCompound tag){
        Speeds ret = new Speeds(tag.getFloat("flySpeedHCap"),tag.getFloat("walkSpeedHCap"),tag.getFloat("swimSpeedHCap"));
        for(MovementTypes t : MovementTypes.values()){
            ret.setSpeed(t,tag.getFloat(t.name()));
        }
        return ret;
    }
}
