package andreyshtern.mp_speed.structs;

public enum MovementDirections {
    VERTICAL, HORIZONTAL;
}
