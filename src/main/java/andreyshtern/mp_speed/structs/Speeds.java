package andreyshtern.mp_speed.structs;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class Speeds {
    private ConcurrentHashMap<MovementTypes, Float> speeds = new ConcurrentHashMap<MovementTypes, Float>();
    private ConcurrentHashMap<MovementTypes, Float> horizontalCaps = new ConcurrentHashMap<MovementTypes, Float>();
    public Speeds(float flySpeed, float walkSpeed, float swimSpeed){
        for(MovementTypes t : MovementTypes.values()){
            speeds.put(t,0f);
        }
        horizontalCaps.put(MovementTypes.SWIM, swimSpeed);
        horizontalCaps.put(MovementTypes.FLY, flySpeed);
        horizontalCaps.put(MovementTypes.WALK, walkSpeed);
    }
    public float getHCaps(MovementTypes m){
        return horizontalCaps.get(m);
    }
    public void setHCaps(MovementTypes m, float value){
        horizontalCaps.put(m,value);
    }
    public void setSpeed(MovementTypes t, float value){
        speeds.put(t,value);
    }
    public float getSpeed(MovementTypes t){
        if(t == MovementTypes.WALK)
            return 0;
        if(speeds.containsKey(t))
            return speeds.get(t);
        return 0;
    }

}
