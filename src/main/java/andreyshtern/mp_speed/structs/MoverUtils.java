package andreyshtern.mp_speed.structs;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;

public class MoverUtils {
    public static void moveEntityWithHeadingOptimised(EntityPlayer player, int x, int y, int times){
        cache = null;
        for(int i = 0; i < times; i++)
            moveOptimised(player,x,y);
    }
    private static void moveOptimised(EntityPlayer player, int x, int y){
        double d0 = player.posX;
        double d1 = player.posY;
        double d2 = player.posZ;

        if (player.capabilities.isFlying && player.ridingEntity == null)
        {
            double d3 = player.motionY;
            float f2 = player.jumpMovementFactor;
            player.jumpMovementFactor = player.capabilities.getFlySpeed();
            cachedMovement(player, x, y);
            player.motionY = d3 * 0.6D;
            player.jumpMovementFactor = f2;
        }
        else
        {
            double savedMotionY = player.motionY;
            cachedMovement(player, x, y);
            player.motionY = savedMotionY;
        }

        player.addMovementStat(player.posX - d0, player.posY - d1, player.posZ - d2);
    }
    private static double[] cache;
    private static void cachedMovement(EntityPlayer player, int x, int y){
        if(cache == null)
            cache = mathShit(player,x,y);
        player.moveEntity(cache[0],cache[1],cache[2]);
        player.prevLimbSwingAmount = player.limbSwingAmount;
        cache[3] = player.posX - player.prevPosX;
        double d1 = player.posZ - player.prevPosZ;
        float f6 = MathHelper.sqrt_double(cache[3] * cache[3] + d1 * d1) * 4.0F;

        if (f6 > 1.0F)
        {
            f6 = 1.0F;
        }

        player.limbSwingAmount += (f6 - player.limbSwingAmount) * 0.4F;
        player.limbSwing += player.limbSwingAmount;
    }
    private static double[] mathShit(EntityPlayer player, int x, int y){

        double d0 = 0;
        double[] ret = new double[4];
        if (player.isInWater() && (!(player instanceof EntityPlayer) || !((EntityPlayer)player).capabilities.isFlying))
        {
            d0 = player.posY;
//            player.moveFlying(x, y, 0.02F);
//            player.moveEntity(player.motionX, player.motionY, player.motionZ);
            player.motionX *= 0.800000011920929D;
            player.motionY *= 0.800000011920929D;
            player.motionZ *= 0.800000011920929D;
            player.motionY -= 0.02D;

            if (player.isCollidedHorizontally && player.isOffsetPositionInLiquid(player.motionX, player.motionY + 0.6000000238418579D - player.posY + d0, player.motionZ))
            {
                player.motionY = 0.30000001192092896D;
            }

        }
        else if (player.handleLavaMovement() && (!(player instanceof EntityPlayer) || !((EntityPlayer)player).capabilities.isFlying))
        {
            d0 = player.posY;
//            player.moveFlying(x, y, 0.02F);
//            player.moveEntity(player.motionX, player.motionY, player.motionZ);
            player.motionX *= 0.5D;
            player.motionY *= 0.5D;
            player.motionZ *= 0.5D;
            player.motionY -= 0.02D;

            if (player.isCollidedHorizontally && player.isOffsetPositionInLiquid(player.motionX, player.motionY + 0.6000000238418579D - player.posY + d0, player.motionZ))
            {
                player.motionY = 0.30000001192092896D;
            }
        }
        else
        {
            float f2 = 0.91F;

            if (player.onGround)
            {
                f2 = player.worldObj.getBlock(MathHelper.floor_double(player.posX), MathHelper.floor_double(player.boundingBox.minY) - 1, MathHelper.floor_double(player.posZ)).slipperiness * 0.91F;
            }

            float f3 = 0.16277136F / (f2 * f2 * f2);
            float f4;

            if (player.onGround)
            {
                f4 = player.getAIMoveSpeed() * f3;
            }
            else
            {
                f4 = player.jumpMovementFactor;
            }

            player.moveFlying(x, y, f4);
            f2 = 0.91F;

            if (player.onGround)
            {
                f2 = player.worldObj.getBlock(MathHelper.floor_double(player.posX), MathHelper.floor_double(player.boundingBox.minY) - 1, MathHelper.floor_double(player.posZ)).slipperiness * 0.91F;
            }

            if (player.isOnLadder())
            {
                float f5 = 0.15F;

                if (player.motionX < (double)(-f5))
                {
                    player.motionX = (double)(-f5);
                }

                if (player.motionX > (double)f5)
                {
                    player.motionX = (double)f5;
                }

                if (player.motionZ < (double)(-f5))
                {
                    player.motionZ = (double)(-f5);
                }

                if (player.motionZ > (double)f5)
                {
                    player.motionZ = (double)f5;
                }

                player.fallDistance = 0.0F;

                if (player.motionY < -0.15D)
                {
                    player.motionY = -0.15D;
                }

                boolean flag = player.isSneaking() && player instanceof EntityPlayer;

                if (flag && player.motionY < 0.0D)
                {
                    player.motionY = 0.0D;
                }
            }

            //player.moveEntity(player.motionX, player.motionY, player.motionZ);

            if (player.isCollidedHorizontally && player.isOnLadder())
            {
                player.motionY = 0.2D;
            }

            if (player.worldObj.isRemote && (!player.worldObj.blockExists((int)player.posX, 0, (int)player.posZ) || !player.worldObj.getChunkFromBlockCoords((int)player.posX, (int)player.posZ).isChunkLoaded))
            {
                if (player.posY > 0.0D)
                {
                    player.motionY = -0.1D;
                }
                else
                {
                    player.motionY = 0.0D;
                }
            }
            else
            {
                player.motionY -= 0.08D;
            }

            player.motionY *= 0.9800000190734863D;
            player.motionX *= (double)f2;
            player.motionZ *= (double)f2;
        }


        ret[0] = player.motionX;
        ret[1] = 0;
        ret[2] = player.motionZ;
        ret[3] = d0;
        return ret;
    }
}
