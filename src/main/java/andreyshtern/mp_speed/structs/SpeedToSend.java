package andreyshtern.mp_speed.structs;

import java.util.List;

public class SpeedToSend {
    private MovementTypes type;
    private MovementDirections dir;
    private int value;
    public SpeedToSend(MovementTypes type, MovementDirections dir, int value){
        this.type = type;
        this.dir = dir;
        this.value = value;
    }

    public MovementTypes getType() {
        return type;
    }

    public int getValue() {
        return value;
    }

    public MovementDirections getDir() {
        return dir;
    }
    public static SpeedToSend getValueFromListOfChanges(List<SpeedToSend> valuesList, MovementDirections dir, MovementTypes type){
        for(SpeedToSend speed : valuesList){
            if(speed.getDir() == dir && speed.getType() == type){
                return speed;
            }
        }
        return null;
    }
    public static void overwriteValue(List<SpeedToSend> valuesList, SpeedToSend speed){
        SpeedToSend value = getValueFromListOfChanges(valuesList,speed.getDir(),speed.getType());
        if(value == null){
            valuesList.add(speed);
            return;
        }

        valuesList.remove(value);
        valuesList.add(speed);

    }
}

