package andreyshtern.mp_speed.structs;

public enum CommandArgType {
    DIRECTION, TYPE, SPEED, NICK;
}
