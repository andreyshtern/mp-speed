package andreyshtern.mp_speed.commands.msg;

import andreyshtern.mp_speed.MpSpeed;
import keelfy.mppermit.server.utils.IMessagesHandler;
import org.apache.logging.log4j.Logger;

public enum Messages implements IMessagesHandler {
    DIR_NOTFOUND, INCORRECT_SPEED, INCORRECT_TYPE, SPEED_SET, WRONG_SYNTAX, WTF, SPEED_TO_LARGE;

    private String code;
    private String serverMessage;

    private Messages() {
        this("");
    }

    private Messages(String serverMessage) {
        this.code = MpSpeed.MODID + ".msg." + this.toString().toLowerCase();
        this.serverMessage = serverMessage;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getDefaultMessage() {
        return this.serverMessage;
    }

    @Override
    public Logger getLogger() {
        return MpSpeed.LOGGER;
    }
}