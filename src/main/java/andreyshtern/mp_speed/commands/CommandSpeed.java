package andreyshtern.mp_speed.commands;

import andreyshtern.mp_speed.MpSpeed;
import andreyshtern.mp_speed.perm.Permissions;
import andreyshtern.mp_speed.structs.CommandArgType;
import andreyshtern.mp_speed.structs.MaxSpeeds;
import andreyshtern.mp_speed.structs.MovementDirections;
import andreyshtern.mp_speed.structs.MovementTypes;
import keelfy.klibrary.server.KServerUtils;
import keelfy.mp_threader.server.commands.ThreadedCommand;
import keelfy.mppermit.permission.Permission;
import keelfy.mppermit.server.utils.PermitMessages;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagFloat;
import andreyshtern.mp_speed.commands.msg.Messages;
import andreyshtern.mp_speed.network.MPSClientPackets;
import andreyshtern.mp_speed.proxy.ServerProxy;
import andreyshtern.mp_speed.utils.SpeedUtil;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CommandSpeed extends ThreadedCommand {

    @Override
    public String getCommandName() {
        return "mp_speed";
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return "/mp_speed";
    }

    @Override
    public List getCommandAliases() {
        List<String> aliases = new ArrayList<String>();
        aliases.add("mpspeed");
        aliases.add("speed");
        return aliases;
    }



    @Override
    public void execute(ICommandSender sender, String[] args) {
        if(!MpSpeed.server)
            return;


        Permission permission = null;
        boolean isOp = KServerUtils.isOp((EntityPlayerMP) sender);
        MaxSpeeds speeds = null;
        if(!isOp){
            permission = Permissions.SPEED.getPermission((EntityPlayerMP) sender, false);
            if(permission == null){
                speeds = new MaxSpeeds();
                speeds.setDefaults();
            }


        }


        String playerName = sender.getCommandSenderName();

        if(args.length != 0)
            playerName = args[0];

        EntityPlayerMP target = KServerUtils.matchPlayer(sender, playerName);

        if (target == null) {
            PermitMessages.PLAYER_NOT_FOUND.send(sender);
            return;
        }

        NBTTagCompound compound = new NBTTagCompound();
        target.writeToNBT(compound);
        if(speeds == null)
            speeds = (isOp) ? new MaxSpeeds(true) : MaxSpeeds.getMaxSpeedsFromMeta(permission.getMetadata());
        MpSpeed.kNetwork.sendTo((EntityPlayerMP) sender, MPSClientPackets.OPEN_GUI);
        MpSpeed.kNetwork.sendTo((EntityPlayerMP) sender, MPSClientPackets.SEND_GUI, playerName, MaxSpeeds.maxSpeedsToNBT(speeds), ServerProxy.getShopUrl(), KServerUtils.isOp((EntityPlayerMP) sender), compound, target.getUniqueID().toString());

    }


    public boolean isThreaded() {
        return true;
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender p_71519_1_) {
        return true;
    }

    @Override
    public List addTabCompletionOptions(ICommandSender sender, String[] args) {
        if(!MpSpeed.server)
            return null;
        List<String> nicknames = new ArrayList<String>();
        for(Object player : sender.getEntityWorld().playerEntities){
            nicknames.add(((EntityPlayerMP)player).getDisplayName());
        }
        List<String> types = new ArrayList<String>();
        for(MovementTypes type : MovementTypes.values()){
            types.add(type.name().toLowerCase());
        }
        List<String> dirs = new ArrayList<String>();
        for(MovementDirections dir : MovementDirections.values()){
            dirs.add(dir.name().toLowerCase());
            dirs.add(dir.name().toLowerCase().substring(0,1));
        }
        switch (args.length){
            case 1:
                return nicknames;
            case 2:
                return types;
            case 3:
                return dirs;
        }

        return null;
    }

    @Override
    public boolean isUsernameIndex(String[] p_82358_1_, int p_82358_2_) {
        return false;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
