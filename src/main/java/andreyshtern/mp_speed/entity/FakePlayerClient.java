package andreyshtern.mp_speed.entity;

import com.mojang.authlib.GameProfile;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.stats.StatBase;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.util.DamageSource;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.Session;
import net.minecraft.world.World;

public class FakePlayerClient extends EntityClientPlayerMP {
    public FakePlayerClient(World world) {
        super(Minecraft.getMinecraft(), world, new Session("", null, null, "legacy"), null, null);
    }
    public FakePlayerClient(World world, GameProfile profile) {
        super(Minecraft.getMinecraft(), world, new Session(profile.getName(), profile.getId().toString(), null, "legacy"), null, null);
    }

    @Override
    public boolean isSneaking() {
        return false;
    }

    @Override
    public boolean canCommandSenderUseCommand(int i, String s) {
        return false;
    }

    @Override
    public ChunkCoordinates getPlayerCoordinates() {
        return new ChunkCoordinates(0, 0, 0);
    }

    @Override
    public void addChatComponentMessage(IChatComponent chatmessagecomponent) {}

    @Override
    public void addStat(StatBase par1StatBase, int par2) {}

    @Override
    public void openGui(Object mod, int modGuiId, World world, int x, int y, int z) {}

    @Override
    public boolean isEntityInvulnerable() {
        return true;
    }

    @Override
    public boolean canAttackPlayer(EntityPlayer player) {
        return false;
    }

    @Override
    public void onDeath(DamageSource source) {
        return;
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
    }

    @Override
    public void travelToDimension(int dim) {
        return;
    }
}
