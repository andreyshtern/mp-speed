package andreyshtern.mp_speed.perm;

import keelfy.mppermit.server.utils.IPermissionsHandler;
import andreyshtern.mp_speed.MpSpeed;

public enum Permissions implements IPermissionsHandler {
    SPEED;

    private final String code;

    private Permissions() {
        this.code = MpSpeed.MODID + "_" + this.toString().toLowerCase();
    }

    @Override
    public String getCode() {
        return this.code;
    }

}
