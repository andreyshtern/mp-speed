package andreyshtern.mp_speed.props;

import andreyshtern.mp_speed.MpSpeed;
import andreyshtern.mp_speed.structs.NBTUtils;
import andreyshtern.mp_speed.structs.Speeds;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;

public class PlayerSpeedProps implements IExtendedEntityProperties{
    private Speeds saved = new Speeds(0.05f, 0.1f, 0.1f);
    public static final String PROP_NAME = MpSpeed.MODID + "_SpeedProperties";

    public static void register(){
        MinecraftForge.EVENT_BUS.register(new Handler());
    }

    public Speeds getSaved() {
        return saved;
    }

    public void setSaved(Speeds saved) {
        this.saved = saved;
    }

    public static class Handler {
        @SubscribeEvent
        public void entityConstruct(EntityEvent.EntityConstructing e) {
            if (e.entity instanceof EntityPlayerMP) {
                if (e.entity.getExtendedProperties(PlayerSpeedProps.PROP_NAME) == null) {
                    e.entity.registerExtendedProperties(PlayerSpeedProps.PROP_NAME, new PlayerSpeedProps());
                }
            }
        }
//        @SubscribeEvent
//        public void playerJoined(EntityJoinWorldEvent e){
//            if(e.entity instanceof EntityPlayerMP){
//                PlayerSpeedProps data = PlayerSpeedProps.get(e.entity);
//
//
//            }
//
//        }

        @SubscribeEvent
        public void onClonePlayer(PlayerEvent.Clone e) {
            if(e.wasDeath) {
                NBTTagCompound compound = new NBTTagCompound();
                PlayerSpeedProps.get(e.original).saveNBTData(compound);
                PlayerSpeedProps.get(e.entityPlayer).loadNBTData(compound);
            }
        }
    }

    public static PlayerSpeedProps get(Entity p) {
        PlayerSpeedProps props = (PlayerSpeedProps) p.getExtendedProperties(PROP_NAME);
        if(props == null){
            p.registerExtendedProperties(PlayerSpeedProps.PROP_NAME, new PlayerSpeedProps());
            props = (PlayerSpeedProps) p.getExtendedProperties(PROP_NAME);
        }
        return props;
    }
    @Override
    public void saveNBTData(NBTTagCompound compound) {
        compound.setTag("speedData", NBTUtils.getNBTFromSpeeds(saved));
    }

    @Override
    public void loadNBTData(NBTTagCompound compound) {

        saved = NBTUtils.getSpeedsFromNBT(compound.getCompoundTag("speedData"));
    }

    public void init(Entity entity, World world) {


    }




}
