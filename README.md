# MoonPower Speed
Created by [Andrey Shapovalov](https://vk.com/andreyshtern)

Simple mod that adds menu and commands for speeding player up.
Contains integrations with MP Permit library.

Features:
* MP Permit Auth
* Client-server sync
* Cool GUI from our designer
* Spagetti code from me

For building, you'll need libraries from `libs` folder (old versions) or from private
Maven repository (current version), login and password from it you will need to get from [keelfy](https://vk.com/keelfy)
